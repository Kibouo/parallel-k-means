#include "utils.hpp"
#include "CSVReader.hpp"
#include "CSVWriter.hpp"
#include <cmath>

namespace utils
{
    Eigen::MatrixXd const parse_data(std::string const& file_name)
    {
        std::ifstream file(file_name);
        file.exceptions(std::ios::badbit);
        CSVReader reader(file);

        auto            first_read = true;
        Eigen::MatrixXd columns_of_datapoints;
        Eigen::VectorXd datapoint;
        while (reader.read(datapoint))
        {
            if (first_read)
            {
                columns_of_datapoints.conservativeResize(datapoint.rows(), 1);
                first_read = false;
            }
            else
            {
                columns_of_datapoints.conservativeResize(
                    Eigen::NoChange, columns_of_datapoints.cols() + 1);
            }

            columns_of_datapoints.col(columns_of_datapoints.cols() - 1)
                = std::move(datapoint);
        }

        file.close();
        return columns_of_datapoints;
    }

    void write_cluster_associations(
        std::string const&                  file_name,
        vector<vector<Eigen::Index>> const& clusters,
        Eigen::Index const                  amount_datapoints,
        bool const                          append)
    {
        // format data
        auto datapoint_cluster_association
            = vector<Eigen::Index>(amount_datapoints);
        for (size_t cluster_index = 0; cluster_index < clusters.size();
             cluster_index++)
        {
            for (auto const& data_index : clusters.at(cluster_index))
            { datapoint_cluster_association.at(data_index) = cluster_index; }
        }

        // actually write it to file
        auto file = append ? std::ofstream(file_name, std::ios_base::app)
                           : std::ofstream(file_name);
        CSVWriter writer(file);
        writer.write(datapoint_cluster_association);
        file.close();
    }

    double squared_euclidean_distance(Eigen::VectorXd const& p,
                                      Eigen::VectorXd const& q)
    {
        double sum = 0;
        for (Eigen::Index i = 0; i < p.rows(); i++)
        { sum += std::pow(p[i] - q[i], 2); }
        return sum;
    }
}