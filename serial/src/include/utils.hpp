#pragma once

#include <Eigen/Dense>
#include <vector>

using std::vector;

namespace utils
{
    /// Returns a matrix in which each row is a datapoint. We store datapoints
    /// per column.
    Eigen::MatrixXd const parse_data(std::string const& file_name);

    void write_cluster_associations(
        std::string const&                  file_name,
        vector<vector<Eigen::Index>> const& clusters,
        Eigen::Index const                  amount_datapoints,
        bool const                          append = false);

    double squared_euclidean_distance(Eigen::VectorXd const& p,
                                      Eigen::VectorXd const& q);
}