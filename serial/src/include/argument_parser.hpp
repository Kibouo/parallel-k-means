#pragma once

#include <unordered_map>

using std::string;

struct ArgumentParser
{
    ArgumentParser(int argc, char* argv[]);
    string const input() const;
    string const output() const;
    uint16_t     k() const;
    uint16_t     repetitions() const;
    uint         seed() const;
    string const trace() const;

    private:
    std::unordered_map<string, string> arguments;
};