#include "argument_parser.hpp"
#include "k_means.hpp"
#include "utils.hpp"
#include "timer.hpp"
#include <iostream>

int main(int argc, char* argv[])
{
    ArgumentParser cli_args(argc, argv);
    KMeans         kmeans(
        utils::parse_data(cli_args.input()), cli_args.k(), cli_args.seed());

    Timer timer = Timer();
    auto [clusters, amount_datapoints]
        = kmeans.run(cli_args.repetitions(), cli_args.trace());
    timer.stop();
    std::cout << timer.duration() << std::endl;


    utils::write_cluster_associations(
        cli_args.output(), clusters, amount_datapoints);

    return 0;
}
