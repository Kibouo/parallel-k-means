#include "k_means.hpp"
#include "utils.hpp"
#include <unordered_set>

KMeans::KMeans(Eigen::MatrixXd const datapoints,
               uint16_t const        k,
               uint const            seed)
    : datapoints(datapoints), k(k)
{
    this->rng       = std::minstd_rand(seed);
    this->centroids = vector<Eigen::VectorXd>(k);
}

std::tuple<vector<vector<Eigen::Index>> const, Eigen::Index const> const
KMeans::run(uint16_t const repetitions, std::string const trace_file)
{
    auto smallest_cost = std::numeric_limits<double>::max();
    vector<vector<Eigen::Index>> best_solution;

    for (size_t i = 0; i < repetitions; i++)
    {
        vector<vector<double>> distances_per_cluster;
        size_t                 nth_iteration = 0;

        this->randomise_centroids();
        this->clusters = vector<vector<Eigen::Index>>(this->k);
        do
        {
            distances_per_cluster = this->assign_data_to_clusters();
            this->update_centroids();

            if (trace_file != "" && i == 0)
            {
                utils::write_cluster_associations(trace_file,
                                                  this->clusters,
                                                  this->datapoints.cols(),
                                                  nth_iteration > 0);
            }

            nth_iteration++;
        }    // TODO: add limiter in case of infinite stealing loop
        while (this->clusters != this->previous_pass_clusters);

        auto cost = this->total_cost(std::move(distances_per_cluster));
        if (cost < smallest_cost)
        {
            smallest_cost = cost;
            best_solution = std::move(this->clusters);
        }
    }

    return { best_solution, this->datapoints.cols() };
}

void KMeans::randomise_centroids()
{
    std::unordered_set<uint> cluster_indices;

    for (auto& centroid : this->centroids)
    {
        // make sure not to accidentally assign the same data entry to 2
        // clusters
        uint_fast32_t index;
        auto          unique_index_found = false;
        while (!unique_index_found)
        {
            index = this->rng() % (uint_fast32_t)this->datapoints.cols();
            if (cluster_indices.find(index) == cluster_indices.end())
            {
                cluster_indices.insert(index);
                unique_index_found = true;
            }
        }

        centroid = this->datapoints.col(index);
    }
}

vector<vector<double>> const KMeans::assign_data_to_clusters()
{
    auto distances_per_cluster = vector<vector<double>>(this->k);

    // store old clusters for comparison later on
    this->previous_pass_clusters = std::move(this->clusters);
    this->clusters               = vector<vector<Eigen::Index>>(this->k);

    // Assign each datapoint to the cluster of which the centroid is the
    // closest. Also store the distances to prevent re-calculation (for the
    // total cost) later on.
    for (Eigen::Index data_index = 0; data_index < this->datapoints.cols();
         data_index++)
    {
        size_t closest_centroid_index;
        auto   smallest_distance = std::numeric_limits<double>::max();
        auto   datapoint         = this->datapoints.col(data_index);

        for (size_t centroid_index = 0; centroid_index < this->centroids.size();
             centroid_index++)
        {
            auto centroid = this->centroids.at(centroid_index);
            auto distance
                = utils::squared_euclidean_distance(datapoint, centroid);

            if (distance < smallest_distance)
            {
                closest_centroid_index = centroid_index;
                smallest_distance      = distance;
            }
        }

        this->clusters.at(closest_centroid_index).push_back(data_index);
        distances_per_cluster.at(closest_centroid_index)
            .push_back(smallest_distance);
    }

    return distances_per_cluster;
}

void KMeans::update_centroids()
{
    // For each cluster we calculate the median of the datapoints and use that
    // as new centroid.
    for (size_t centroid_index = 0; centroid_index < this->centroids.size();
         centroid_index++)
    {
        auto cluster_size = this->clusters.at(centroid_index).size();
        auto summed_datapoints
            = Eigen::VectorXd(this->centroids.at(centroid_index).size())
                  .setZero();

        for (auto const& datapoint_index : this->clusters.at(centroid_index))
        { summed_datapoints += this->datapoints.col(datapoint_index); }

        this->centroids.at(centroid_index) = summed_datapoints / cluster_size;
    }
}

double KMeans::total_cost(
    vector<vector<double>> const distances_per_cluster) const
{
    double sum = 0;
    for (auto const& distances : distances_per_cluster)
    {
        for (auto const& distance : distances) { sum += distance; }
    }

    return sum;
}