\documentclass[a4paper, 11pt]{article} 

\title{Analyse K-means:\\OpenMP} 
\author{Mihály Csonka (1644219)} 
\date{\today}

\usepackage[parfill]{parskip}
\usepackage[
    style=numeric,
    backend=biber,
    isbn=true,
    url=true,
    datamodel=archive
]{biblatex}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{caption}
\usepackage{csquotes}
\usepackage[toc,page]{appendix}
\usepackage[hidelinks]{hyperref}
\usepackage{geometry}
\geometry{margin=1.2in}

\begin{document} 
\maketitle

\section{Parallelisation choices}
The K-means algorithm lends itself to parallelism despite being an iterative optimization algorithm. This is due to almost each sub-step being a SIMD operation. Therefore, the majority of the parallelisation is done over loops. 

The dataset is immutable. Thus, none of the accesses to it have to be synchronised. 

\subsection{Repetitions}
The repetitions of the algorithm are embarrassingly parallel. Each is an instance of K-means but with a different input in the form of random initial centroids.

As not every repetition of the algorithm takes the same time, the parallelisation scheduler is set to be \texttt{dynamic}. Every repetition is independent and the repetition index does not index anything prone to false sharing. The chunk size is thus \texttt{1}.

The reason for multiple repetitions is finding the clustering with minimal cost. To calculate this minimal cost and store the corresponding clustering along with it, we wrote a custom reduction function for OpenMP to use.

Lastly, the random number generator is not thread safe. We created one per repetition with the seed \texttt{default\_seed + nth\_repetition} to prevent having to synchronise access to it. 

\subsection{Iterative optimization}
The iterative optimization, consisting of the re-assignment of items to clusters and then the updating of centroids, is unfit for parallelisation. The two steps are dependent on each other.

\subsection{Randomisation of centroids}
The \texttt{randomise\_centroids} function chooses \texttt{k} random but unique samples within the dataset. The uniqueness requirement requires samples to be stored. This list would have to be synchronised. In the end the function was not parallelised as tests showed the overhead outweighed the gains.

\subsection{Assigning data to clusters}
The \texttt{assign\_data\_to\_clusters} function checks for every sample the distance to each centroid. It then assigns the sample to the closest cluster. 

The outer loop, which iterates over each sample, has been parallelised as finding the closest cluster works the same for every sample. The scheduler here is \texttt{static} as every sample's association calculation requires iterating over all centroids.

The inner loop, which checks every centroid for a given sample, has not been parallelised. It only checks whether the current distance is shorter than the previous. Tests also confirmed a non-existent improvement in execution time when parallelised.

\subsection{Distance calculation}
Unlike the inner loop mentioned above, the distance calculating function used by it has been parallelised. The squared Euclidean distance is reducible using OpenMP. Each dimension undergoes the same mathematical operations thus the \texttt{static} scheduler is used.

\subsection{Updating centroids}
The \texttt{update\_centroids} function calculates the new centroids. This calculation is independent per cluster. It involves the calculation of the median data point and can thus be parallelised with a \texttt{static} scheduler.

\subsection{Total cost}
The calculation of the total cost of a clustering is a simple double loop over pre-calculated costs. We did not parallelise this function as tests showed the overhead to outweigh the gains.

\section{Speed up per core count}
We tested two scenarios with equivalent time complexity using the \texttt{simple1} dataset: 10 repetitions and \texttt{k = 20}, and vice versa. The speed up per core count is shown in Figure~\ref{speedup_rep10_k20} and Figure~\ref{speedup_rep20_k10} respectively.

We observe that the speed up when using a higher repetition count is 5 to 10 units higher from 8 cores onwards.

We also notice that the speed up mostly flattens out after 16 cores. We assume the following to be happening: parallelising over repetitions results in every repetition being done in one thread. At this point, due to the serial nature of the K-means algorithm, the execution time is dominated by the speed of the repetition's main thread even if the serial parts are parallelised internally.

In conclusion, our implementation scales well with the amount of repetitions. However, it scales less well with the other parameters. Also, using many more cores than repetitions has no use as the extra cores will be waiting to get work assigned to them by a repetition's master thread.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.45]{fig_speedup_rep10_k20.png}
    \caption{Speed up per core count for repetitions = 10 and k = 20; A superlinear speed up, likely due to caching, is achieved when using up to 16 cores. We also see the speed up increasing all over the board. However, after 16 cores the increase slows down heavily.}\label{speedup_rep10_k20}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.45]{fig_speedup_rep20_k10.png}
    \caption{Speed up per core count for repetitions = 20 and k = 10; Idem to Figure~\ref{speedup_rep10_k20}. However, the speed up is consistently higher despite the configuration having the same time complexity.}\label{speedup_rep20_k10}
\end{figure}

\section{Serial fraction}
After having parsed the data, our implementation starts to calculate the repetitions of the K-means algorithm. No extra initialisation and no post-processing, after reduction of the results, is required. As stated earlier, performing repetitions is embarrassingly parallel. Thus, there is no serial part to our program.
The first test run using the \texttt{simple1} dataset with \texttt{k = 20}, 10 repetitions, and 8 cores gave us following values:
\[T_s = 0ns\]
\[T_p = 2.62876e^{+11}ns\]
\[f = T_s / T_p = 0\]

\section{Parallel overhead}
In the above section \(T_p\) was for 8 cores. Thus, the prediction for 16 cores would be \(T_p * 8/16 = 1.31438e^{+11}ns\). Our data shows a parallel runtime of \(1.47957e^{+10}ns\). This puts the overhead to \(-1.6642e^{+11}ns\). However, in our second run the overhead evaluates to \(8.00275e^{+9}ns\). In other words, our implementation does not have much overhead as it can be remedied by the increase in performance, for the given parameters. The overhead stems from reductions and thread management.

\section{Predictions for more cores}
Our implementation follows weak scaling. The main improvement compared to the serial implementation comes from parallelising the repetitions. Secondary improvements come from parallelising the iterations over the dataset, stored in various forms.
Thus, we do not expect a \textit{significant} improvement in execution time given more cores. What we do expect is that the execution time will stay roughly the same given a higher amount of repetitions and, to some degree, a higher \texttt{k} and bigger dataset.

When increasing the number of cores to infinite, the speed up would become \(1/s\) with \(s = T_s/T_s+(T_p*N)\). Given \(T_s = 0\) as explained above, we would achieve a speed up approaching \(\infty \). However, this will never happen due to several factors:
\begin{itemize}
    \item overhead will have a bigger impact given a higher number of cores.
    \item our implementation loses a lot of its \textit{power} when the number of repetitions do not increase along with the number of cores.
    \item the K-means algorithm itself is, by definition, serial. Thus, while the individual steps can be parallelised, they will have to wait for the previous step to finish.
\end{itemize}

\section{Possible extra parallelisation}
Following are possible extra parallelisation which could be implemented.

\subsection{Eigen vector operations}
Operations such as dividing all elements of a vector by a factor or adding vectors by summing corresponding dimensions are parallelised by the Eigen library by default. We disabled this for the assignment but didn't reimplement it manually.

\subsection{Comparison of clusterings}
Clusters are stored as a vector of sets of indices to samples in the original dataset. The comparison of clusterings could be parallelised by reimplementing the standard library's \texttt{==} operator for vectors. 

\end{document}