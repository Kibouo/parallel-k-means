#include "k_means.hpp"
#include "utils.hpp"
#include <iostream>
#include <omp.h>
#include <unordered_set>

KMeans::KMeans(Eigen::MatrixXd const datapoints,
               uint16_t const        k,
               uint const            seed)
    : datapoints(datapoints), k(k), seed(seed)
{
}

std::tuple<vector<vector<Eigen::Index>> const, Eigen::Index const> const
KMeans::run(uint16_t const repetitions, std::string const trace_file)
{
    KmeansIterationSolution best_solution = KmeansIterationSolution();
#pragma omp declare reduction(mincostsolution:KmeansIterationSolution \
                              : omp_out = MinCostSolution(omp_out, omp_in))
#pragma omp         parallel for schedule(dynamic, 1) reduction(mincostsolution \
                                                        : best_solution)
    for (size_t i = 0; i < repetitions; i++)
    {
        std::minstd_rand             rng = std::minstd_rand(this->seed + i);
        vector<vector<Eigen::Index>> clusters;
        vector<vector<Eigen::Index>> previous_pass_clusters;
        vector<Eigen::VectorXd>      centroids(this->k);
        vector<vector<double>>       distances_per_cluster;
        size_t                       nth_iteration = 0;

        // This loop is serial, within a run, by definition.
        this->randomise_centroids(centroids, move(rng));
        clusters = vector<vector<Eigen::Index>>(this->k);
        do
        {
            distances_per_cluster = this->assign_data_to_clusters(
                clusters, previous_pass_clusters, centroids);
            this->update_centroids(clusters, centroids);

            // No need to make critical as it will only ever be run by the 1st
            // repetition/thread by definition.
            if (trace_file != "" && i == 0)
            {
                utils::write_cluster_associations(trace_file,
                                                  clusters,
                                                  this->datapoints.cols(),
                                                  nth_iteration > 0);
            }

            nth_iteration++;
        }    // TODO: add limiter in case of infinite stealing loop
        while (clusters != previous_pass_clusters);

        best_solution.cost = this->total_cost(std::move(distances_per_cluster));
        best_solution.clusters = std::move(clusters);
    }

    return { best_solution.clusters, this->datapoints.cols() };
}

void KMeans::randomise_centroids(vector<Eigen::VectorXd>& centroids,
                                 std::minstd_rand&&       rng)
{
    std::unordered_set<uint> cluster_indices;

    // Tested: parallelising this loop is always detrimental to performance.
    // Probably due to every thread having to access the ``cluster_indices``
    // hashmap.
    for (auto& centroid : centroids)
    {
        // make sure not to accidentally assign the same data entry to 2
        // clusters
        uint_fast32_t index;
        auto          unique_index_found = false;
        while (!unique_index_found)
        {
            index = rng() % (uint_fast32_t)this->datapoints.cols();
            if (cluster_indices.find(index) == cluster_indices.end())
            {
                cluster_indices.insert(index);
                unique_index_found = true;
            }
        }

        centroid = this->datapoints.col(index);
    }
}

vector<vector<double>> const KMeans::assign_data_to_clusters(
    vector<vector<Eigen::Index>>&  clusters,
    vector<vector<Eigen::Index>>&  previous_pass_clusters,
    vector<Eigen::VectorXd> const& centroids)
{
    auto distances_per_cluster = vector<vector<double>>(this->k);

    // store old clusters for comparison later on
    previous_pass_clusters        = std::move(clusters);
    clusters                      = vector<vector<Eigen::Index>>(this->k);
    Eigen::Index const data_items = this->datapoints.cols();
    // Assign each datapoint to the cluster of which the centroid is the
    // closest. Also store the distances to prevent re-calculation (for the
    // total cost) later on.
#pragma omp parallel for schedule(static, 1)
    for (Eigen::Index data_index = 0; data_index < data_items; data_index++)
    {
        size_t closest_centroid_index;
        auto   smallest_distance = std::numeric_limits<double>::max();
        auto   datapoint         = this->datapoints.col(data_index);

        // Tested: no improvement when parallelising this loop. There isn't much
        // work being done here. The work is in the distance calculation.
        for (size_t centroid_index = 0; centroid_index < centroids.size();
             centroid_index++)
        {
            auto centroid = centroids.at(centroid_index);
            auto distance
                = utils::squared_euclidean_distance(datapoint, centroid);

            if (distance < smallest_distance)
            {
                closest_centroid_index = centroid_index;
                smallest_distance      = distance;
            }
        }

// TODO: could this be done with a reducer?
#pragma omp critical
        {
            clusters.at(closest_centroid_index).push_back(data_index);
            distances_per_cluster.at(closest_centroid_index)
                .push_back(smallest_distance);
        }
    }

    return distances_per_cluster;
}

void KMeans::update_centroids(vector<vector<Eigen::Index>> const& clusters,
                              vector<Eigen::VectorXd>&            centroids)
{
    auto const& datapoint_dimensions = centroids.at(0).size();
    // For each cluster we calculate the median of the datapoints and use that
    // as new centroid.
    // Each update should take roughly the same time so we use static.
#pragma omp parallel for schedule(static, 1)
    for (size_t centroid_index = 0; centroid_index < centroids.size();
         centroid_index++)
    {
        auto cluster_size = clusters.at(centroid_index).size();
        auto summed_datapoints
            = Eigen::VectorXd(datapoint_dimensions).setZero();

        for (auto const& datapoint_index : clusters.at(centroid_index))
        { summed_datapoints += this->datapoints.col(datapoint_index); }

        centroids.at(centroid_index) = summed_datapoints / cluster_size;
    }
}

double KMeans::total_cost(
    vector<vector<double>> const distances_per_cluster) const
{
    // Parallelising this only gives overhead. Speed decrease in factor of x10.
    double sum = 0;
    for (auto const& distances : distances_per_cluster)
    {
        for (auto const& distance : distances) { sum += distance; }
    }

    return sum;
}