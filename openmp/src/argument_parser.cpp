#include "argument_parser.hpp"

uint const STUDENT_NUMBER = 1644219;

ArgumentParser::ArgumentParser(int argc, char* argv[])
{
    for (int i = 1; i < argc; i += 2)
    { this->arguments.insert_or_assign(argv[i], argv[i + 1]); }
}

string const ArgumentParser::input() const
{
    return this->arguments.at("--input");
}

string const ArgumentParser::output() const
{
    return this->arguments.at("--output");
}

uint16_t ArgumentParser::k() const { return stoi(this->arguments.at("--k")); }

uint16_t ArgumentParser::repetitions() const
{
    return stoi(this->arguments.at("--repetitions"));
}

uint ArgumentParser::seed() const
{
    try
    {
        return stoi(this->arguments.at("--seed"));
    }
    catch (std::out_of_range const&)
    {
        return STUDENT_NUMBER;
    }
}

string const ArgumentParser::trace() const
{
    try
    {
        return this->arguments.at("--trace");
    }
    catch (std::out_of_range const&)
    {
        return "";
    }
}
