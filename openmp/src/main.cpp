#include "argument_parser.hpp"
#include "k_means.hpp"
#include "timer.hpp"
#include "utils.hpp"
#include <iostream>
#include <omp.h>

int main(int argc, char* argv[])
{
    omp_set_max_active_levels(64);
    omp_set_dynamic(64);

    ArgumentParser cli_args(argc, argv);
    KMeans         kmeans(
        utils::parse_data(cli_args.input()), cli_args.k(), cli_args.seed());

    Timer timer = Timer();
    auto  pair  = kmeans.run(cli_args.repetitions(), cli_args.trace());
    timer.stop();
    std::cout << omp_get_max_threads() << " " << timer.duration() << std::endl;

    auto const clusters          = std::get<0>(pair);
    auto const amount_datapoints = std::get<1>(pair);

    utils::write_cluster_associations(
        cli_args.output(), clusters, amount_datapoints);

    return 0;
}
