#pragma once

#include <Eigen/Dense>
#include <random>
#include <tuple>

using std::vector;

class KMeans
{
    Eigen::MatrixXd const datapoints;
    uint16_t const        k;
    uint const            seed;

    void randomise_centroids(vector<Eigen::VectorXd>& centroids,
                             std::minstd_rand&&       rng);
    // Returns, per cluster, the distances of the datapoints to their respective
    // centroids.
    vector<vector<double>> const assign_data_to_clusters(
        vector<vector<Eigen::Index>>&  clusters,
        vector<vector<Eigen::Index>>&  previous_pass_clusters,
        vector<Eigen::VectorXd> const& centroids);
    void update_centroids(vector<vector<Eigen::Index>> const& clusters,
                          vector<Eigen::VectorXd>&            centroids);
    // Calculate the total cost of the current datapoint-to-cluster assignment
    // configuration.
    double total_cost(vector<vector<double>> const distances_per_cluster) const;

    public:
    KMeans(Eigen::MatrixXd const datapoints, uint16_t const k, uint const seed);
    std::tuple<vector<vector<Eigen::Index>> const, Eigen::Index const> const
    run(uint16_t const repetitions, std::string const trace_file);
};