import os

DATA_PATH = "./data/simple1/runs_per_process_count_with_t_parallel/"
OUTPUT_PATH = "./data/simple1/"

dirs_iter = os.listdir(DATA_PATH)
dirs_iter.sort(key=lambda x: int(x.split("_")[1]))
for dir in dirs_iter:
    processors = dir.split("_")[1]

    for mpi_file in os.listdir(DATA_PATH+dir):
        filename, file_extension = os.path.splitext(mpi_file)
        if file_extension != ".csv":
            continue
        if filename == "output":
            continue
        with open(DATA_PATH+dir+"/"+mpi_file) as data_file:
            for (line_nr, line) in enumerate(data_file.readlines()):
                with open(OUTPUT_PATH+filename+"-run"+str(line_nr+1)+file_extension, "a") as write_file:
                    line = line.split(",")[0] + " " + line.split(",")[1] + '\n'
                    write_file.write(line)
