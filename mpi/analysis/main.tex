\documentclass[a4paper, 11pt]{article} 

\title{Analyse K-means:\\MPI} 
\author{Mihály Csonka (1644219)} 
\date{\today}

\usepackage[parfill]{parskip}
\usepackage[
    style=numeric,
    backend=biber,
    isbn=true,
    url=true,
    datamodel=archive
]{biblatex}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{caption}
\usepackage{csquotes}
\usepackage[toc,page]{appendix}
\usepackage[hidelinks]{hyperref}
\usepackage{geometry}
% \geometry{margin=1.0in}
\hyphenpenalty=10000

\begin{document} 
\maketitle

\section{Parallelisation choices}
Due to previous implementations we know what parts of the k-means algorithm are the heaviest to calculate and fit to be parallelised.

MPI starts copies of the program in multiple processes. This results in a copy of the immutable dataset being stored for each process. Cluster associations and centroids will also be stored for each process. However, these will be calculated in a distributed fashion as explained further on.

\subsection{Not parallelised}
\subsubsection{Amount of repetitions}
Unlike with OpenMP, we decided to not parallelise the number of repetitions. Giving each process the responsibility over a set of repetitions would namely limit the available processing power. This is because the actual computationally heavy parts would be limited by the capabilities of a single processor. I.e. the algorithm would only scale with the number of repetitions. Unless it is known beforehand that the usage will be limited to a low number of samples and clusters, and a higher amount of repetitions, parallelising this way would only harm the general usability.

\subsubsection{Random initialisation of centroids}
Each process has a copy of the dataset and a copy of the seed. Using this, the initial random centroids can be chosen independently per process. This has the advantage that no communication (read: overhead) is required between processes.

\subsection{Parallelised}
\subsubsection{Assigning samples to clusters}
The function \texttt{KMeans::assign\_data\_to\_clusters()} is the heaviest in terms of processing power required, due to calculation of distances between samples and centroids. This has to happen for each sample-centroid pair. Distributing this workload over all processes seems a good choice.

Given that each process knows its rank, all samples, and all centroids, the amount of samples/work can be divided into roughly equally sized chunks. After the calculations of the new cluster associations are finished, a reduction and broadcast are used to let every process know the full set of results. Broadcasting is required as each process requires knowledge of their sample's cluster associations in Subsubsection \ref{update_centrs}.

\subsubsection{Updating centroids}\label{update_centrs}
The function \texttt{KMeans::update\_centroids()} calculates the new centroids given the relevant associated samples. While not as computationally expensive, the amount of work (\texttt{amt\_samples * amt\_centroids}) is the same as for assigning samples to clusters. Work is again distributed by dividing the amount of samples in roughly equal chunks for each process. The results are reduced and broadcast afterwards.

Unlike in our CUDA implementation, the overhead of parallelising this function did not outweigh the gains.

\section{Speedup per process}\label{spp}
For each multiple of processes and combinations of parameters we ran several duplicate tests to try and compensate for outliers.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.44]{fig_speedup_simple1.png}
    \caption{Speedup plot for the \texttt{simple1} dataset. Parameters used to create measurements were \(k = 30\) and 30 repetitions.}\label{plot}
\end{figure}

On Figure \ref{plot} we notice that at 32 processes the curve starts to flatten. The \texttt{sample1} dataset contains 5000 samples. This means that the average process has to go over chunks of \texttt{5000 / amt\_processes} samples. More processes thus means less work per individual. However, more processes also means more overhead due to communication. For this dataset it seems the sweet spot, i.e. the point where the overhead from communication does not yet overtake the gains from distributing the work, lies at \(5000 / 64 \approx 78\) samples per process.

We also made some measurements with the same parameters on the \texttt{100000} dataset. In Figure \ref{plot2} we notice that with more samples per process, even at 128 processes, the overhead does not yet outweigh the gains of distributed computation.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.44]{fig_speedup_100000.png}
    \caption{Speedup plot for the \texttt{100000} dataset. The parameters were again \(k = 30\) and 30 repetitions. Lower process counts were omitted.}\label{plot2}
\end{figure}

\section{Serial fraction}
For the \texttt{simple1} dataset with parameters \(k = 30\), 30 repetitions, and 8 processes we got, for the first test run\footnote{These execution times are representative for the implementation and used parameters. I.e. these are not outliers.}; \(T_s = 5*10^{-4}s = 0.5ms\), \(T_p = 2.686s\), and as result \(f = 0.9998\).

\section{Parallel overhead}
By doubling the amount of processes to 16 we would expect \(T_p\) to be halved. However, we observe \(T_p = 1.616s\). This results in an overhead of \(T_o = 0.273s\). This overhead is due to reduction and broadcasting of data. More precisely, the sending of data and waiting for data to be received.

\section{Predictions for infinite processors}
In Section \ref{spp} we have seen that blindly increasing the number of processes is detrimental. When increasing the number of processors we have to increase the amount of work as well. Otherwise the overhead will catch up and result in negative gains. Having a truly infinite amount of processors would thus require a bigger infinite amount of samples.

As mentioned before, our implementation divides the number of samples over the amount of processes. Each process has work equal to \texttt{chunk\_samples * amt\_clusters}. This means that if the amount of samples is similar to the amount of processes, an appropriate amount of clusters will be needed as to not waste processor capabilities.

In short: when increasing the number of processors our implementation scales well with the amount of samples, scales with the amount of clusters given certain conditions, and does not scale well with the amount of repetitions.

\section{Improvements}
We could not think of any improvement to be made using MPI.

\clearpage
\section{Evaluation}
Each implementation took around 15-20 hours of work. OpenMP took the most time while MPI took the least. 

Work consisted of three main parts on which roughly equal time was spent: 
\begin{itemize}
    \item parallelising the code. 
    \item running a test, interpreting results, tweaking parameters or even code, and then repeating the cycle.
    \item writing the report.
\end{itemize}

In general the difficulty was on an appropriate level. However, for the CUDA implementation, due to its parallel nature, it was not always clear as to what was expected of us.

Getting feedback as a class, about our parallelisation choices and not about code quality, after each implementation would also have been beneficial. Seeing how others tackled the problem, discussing which approaches were appropriate and which were not, and \textit{why} would have been helpful. In the report we were namely asked to think about our own solution, but our approach could be lacking severally and/or we could be wrong about our thinking process.

\end{document}
