#include "k_means.hpp"
#include "timer.hpp"
#include "utils.hpp"
#include <mpi.h>
#include <unordered_set>

KMeans::KMeans(Eigen::MatrixXd const datapoints,
               uint16_t const        k,
               uint const            seed)
    : datapoints(datapoints), k(k)
{
    this->rng       = std::minstd_rand(seed);
    this->centroids = Eigen::MatrixXd(datapoints.rows(), k);
}

std::tuple<vector<vector<Eigen::Index>>, Eigen::Index, double> KMeans::run(
    uint16_t const repetitions, std::string const trace_file)
{
    int rank = -1;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    double parallel_time = 0.0;
    auto   smallest_cost = std::numeric_limits<double>::max();
    vector<vector<Eigen::Index>> best_solution{};

    for (size_t i = 0; i < repetitions; i++)
    {
        auto   cur_rep_cost  = smallest_cost;
        size_t nth_iteration = 0;

        this->randomise_centroids();
        this->clusters = vector<vector<Eigen::Index>>(this->k);
        do
        {
            Timer timer  = Timer();
            cur_rep_cost = this->assign_data_to_clusters();
            this->update_centroids();
            timer.stop();
            parallel_time += timer.duration();

            if (trace_file != "" && i == 0 && rank == 0)
            {
                utils::write_cluster_associations(trace_file,
                                                  this->clusters,
                                                  this->datapoints.cols(),
                                                  nth_iteration > 0);
            }

            nth_iteration++;
        }    // TODO: add limiter in case of infinite stealing loop
        while (this->clusters != this->previous_pass_clusters);

        if (rank == 0)
        {
            if (cur_rep_cost < smallest_cost)
            {
                smallest_cost = cur_rep_cost;
                best_solution = std::move(this->clusters);
            }
        }
    }

    return std::make_tuple(
        best_solution, this->datapoints.cols(), parallel_time);
}

void KMeans::randomise_centroids()
{
    std::unordered_set<uint> cluster_indices;

    for (Eigen::Index centroid_index = 0;
         centroid_index < this->centroids.cols();
         centroid_index += 1)
    {
        auto centroid = this->centroids.col(centroid_index);
        // make sure not to accidentally assign the same data entry to 2
        // clusters
        uint_fast32_t index;
        auto          unique_index_found = false;
        while (!unique_index_found)
        {
            index = this->rng() % (uint_fast32_t)this->datapoints.cols();
            if (cluster_indices.find(index) == cluster_indices.end())
            {
                cluster_indices.insert(index);
                unique_index_found = true;
            }
        }

        centroid = this->datapoints.col(index);
    }
}

double KMeans::assign_data_to_clusters()
{
    // There are a lot of datapoints which have to be processed independently.
    // Our approach is to divide work over multiple nodes.
    auto       work_data = utils::calc_work_size(this->datapoints.cols());
    auto const work_size_of_this_node    = std::get<0>(work_data);
    auto const work_size_of_average_node = std::get<1>(work_data);
    auto const rank                      = std::get<2>(work_data);

    // store old clusters for comparison later on
    this->previous_pass_clusters = std::move(this->clusters);
    this->clusters               = vector<vector<Eigen::Index>>(this->k);

    auto           distances_per_cluster = vector<vector<double>>(this->k);
    vector<size_t> node_local_centroid_per_sample(this->datapoints.cols(), 0);
    // Assign each datapoint to the cluster of which the centroid is the
    // closest. Also store the distances to prevent re-calculation (for the
    // total cost) later on.
    for (Eigen::Index data_index = work_size_of_average_node * rank;
         data_index < work_size_of_average_node * rank + work_size_of_this_node;
         data_index++)
    {
        size_t closest_centroid_index;
        auto   smallest_distance = std::numeric_limits<double>::max();
        auto   datapoint         = this->datapoints.col(data_index);

        for (Eigen::Index centroid_index = 0;
             centroid_index < this->centroids.cols();
             centroid_index++)
        {
            auto centroid = this->centroids.col(centroid_index);
            auto distance
                = utils::squared_euclidean_distance(datapoint, centroid);

            if (distance < smallest_distance)
            {
                closest_centroid_index = centroid_index;
                smallest_distance      = distance;
            }
        }

        node_local_centroid_per_sample.at(data_index) = closest_centroid_index;
        distances_per_cluster.at(closest_centroid_index)
            .push_back(smallest_distance);
    }
    auto node_local_cost = this->total_cost(std::move(distances_per_cluster));

    // reduce & broadcast results
    auto           cost = std::numeric_limits<double>::max();
    vector<size_t> centroid_per_sample(this->datapoints.cols());

    MPI_Reduce(
        &node_local_cost, &cost, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Allreduce(node_local_centroid_per_sample.data(),
                  centroid_per_sample.data(),
                  this->datapoints.cols(),
                  MPI_UNSIGNED_LONG,
                  MPI_SUM,
                  MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);

    // it's complicated to work with 2d vecs with dynamic row lengts in MPI,
    // which our serial implementation used for clusters. To prevent rewriting
    // of handling of the 2d vecs we let MPI send 1d flattened representations
    // and unfold it here to 2d.
    for (size_t sample_idx = 0; sample_idx < centroid_per_sample.size();
         sample_idx += 1)
    {
        this->clusters.at(centroid_per_sample.at(sample_idx))
            .push_back(sample_idx);
    }

    return cost;
}

void KMeans::update_centroids()
{
    // Similar to the ``KMeans::assign_data_to_clusters()`` MPI implementation
    // we divide the amount of work over the amount of processes.
    auto       work_data = utils::calc_work_size(this->datapoints.cols());
    auto const work_size_of_this_node    = std::get<0>(work_data);
    auto const work_size_of_average_node = std::get<1>(work_data);
    auto const rank                      = std::get<2>(work_data);

    auto node_local_centroids
        = Eigen::MatrixXd(datapoints.rows(), this->k).setZero();
    for (Eigen::Index centroid_index = 0;
         centroid_index < this->centroids.cols();
         centroid_index += 1)
    {
        auto cluster_size = this->clusters.at(centroid_index).size();
        auto summed_datapoints
            = Eigen::VectorXd(this->centroids.rows()).setZero();

        for (auto const& datapoint_index : this->clusters.at(centroid_index))
        {
            if (datapoint_index >= work_size_of_average_node * rank
                && datapoint_index < work_size_of_average_node * rank
                                         + work_size_of_this_node)
            { summed_datapoints += this->datapoints.col(datapoint_index); }
        }

        node_local_centroids.col(centroid_index)
            = summed_datapoints / cluster_size;
    }

    MPI_Allreduce(node_local_centroids.data(),
                  this->centroids.data(),
                  node_local_centroids.size(),
                  MPI_DOUBLE,
                  MPI_SUM,
                  MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
}

double KMeans::total_cost(
    vector<vector<double>> const distances_per_cluster) const
{
    double sum = 0;
    for (auto const& distances : distances_per_cluster)
    {
        for (auto const& distance : distances) { sum += distance; }
    }

    return sum;
}