#include "argument_parser.hpp"
#include "k_means.hpp"
#include "timer.hpp"
#include "utils.hpp"
#include <iostream>
#include <mpi.h>

int main(int argc, char* argv[])
{
    MPI_Init(&argc, &argv);

    ArgumentParser cli_args(argc, argv);
    KMeans         kmeans(
        utils::parse_data(cli_args.input()), cli_args.k(), cli_args.seed());

    Timer timer = Timer();
    auto  pair  = kmeans.run(cli_args.repetitions(), cli_args.trace());
    timer.stop();

    auto const clusters          = std::get<0>(pair);
    auto const amount_datapoints = std::get<1>(pair);
    auto const parallel_time     = std::get<2>(pair);

    int rank      = -1;
    int amt_nodes = -1;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &amt_nodes);
    if (rank == 0)
    {
        std::cout << amt_nodes << "," << timer.duration() << ","
                  << parallel_time << std::endl;

        utils::write_cluster_associations(
            cli_args.output(), clusters, amount_datapoints);
    }

    MPI_Finalize();
    return 0;
}
