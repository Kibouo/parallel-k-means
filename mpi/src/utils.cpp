#include "utils.hpp"
#include "CSVReader.hpp"
#include "CSVWriter.hpp"
#include <cmath>
#include <mpi.h>

Eigen::MatrixXd utils::parse_data(std::string const& file_name)
{
    std::ifstream file(file_name);
    file.exceptions(std::ios::badbit);
    CSVReader reader(file);

    auto            first_read = true;
    Eigen::MatrixXd columns_of_datapoints;
    Eigen::VectorXd datapoint;
    while (reader.read(datapoint))
    {
        if (first_read)
        {
            columns_of_datapoints.conservativeResize(datapoint.rows(), 1);
            first_read = false;
        }
        else
        {
            columns_of_datapoints.conservativeResize(
                Eigen::NoChange, columns_of_datapoints.cols() + 1);
        }

        columns_of_datapoints.col(columns_of_datapoints.cols() - 1)
            = std::move(datapoint);
    }

    file.close();
    return columns_of_datapoints;
}

void utils::write_cluster_associations(
    std::string const&                  file_name,
    vector<vector<Eigen::Index>> const& clusters,
    Eigen::Index const                  amount_datapoints,
    bool const                          append)
{
    // format data
    auto datapoint_cluster_association
        = vector<Eigen::Index>(amount_datapoints);
    for (size_t cluster_index = 0; cluster_index < clusters.size();
         cluster_index++)
    {
        for (auto const& data_index : clusters.at(cluster_index))
        { datapoint_cluster_association.at(data_index) = cluster_index; }
    }

    // actually write it to file
    auto file = append ? std::ofstream(file_name, std::ios_base::app)
                       : std::ofstream(file_name);
    CSVWriter writer(file);
    writer.write(datapoint_cluster_association);
    file.close();
}

double utils::squared_euclidean_distance(Eigen::VectorXd const& p,
                                         Eigen::VectorXd const& q)
{
    double sum = 0;
    for (Eigen::Index i = 0; i < p.rows(); i++)
    { sum += std::pow(p[i] - q[i], 2); }
    return sum;
}

std::tuple<Eigen::Index, Eigen::Index, int> utils::calc_work_size(
    size_t amt_data)
{
    Eigen::Index work_size_of_this_node    = 0;
    Eigen::Index work_size_of_average_node = 0;
    int          rank                      = -1;
    {
        int amt_nodes = -1;
        MPI_Comm_size(MPI_COMM_WORLD, &amt_nodes);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        work_size_of_this_node    = (int)(amt_data / amt_nodes);
        work_size_of_average_node = work_size_of_this_node;
        // work might not be evenly distributable. Last node gets a little bit
        // extra. We do not evenly distribute (every node gets 1 item) to
        // prevent a lot of small work to create a lot of overhead due to the
        // communication needed.
        if (rank == amt_nodes - 1)
        { work_size_of_this_node += amt_data % amt_nodes; }
    }

    return { work_size_of_this_node, work_size_of_average_node, rank };
}