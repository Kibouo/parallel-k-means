#pragma once

#include <Eigen/Dense>
#include <tuple>
#include <vector>

using std::vector;

namespace utils
{
    /// Returns a matrix in which each row is a datapoint. We store datapoints
    /// per column.
    Eigen::MatrixXd parse_data(std::string const& file_name);

    void write_cluster_associations(
        std::string const&                  file_name,
        vector<vector<Eigen::Index>> const& clusters,
        Eigen::Index const                  amount_datapoints,
        bool const                          append = false);

    double squared_euclidean_distance(Eigen::VectorXd const& p,
                                      Eigen::VectorXd const& q);

    // Calculates amount of data that a node has to process when that data is
    // distributed over all nodes.
    // Returns (node_work, avg_work, rank)
    std::tuple<Eigen::Index, Eigen::Index, int> calc_work_size(size_t amt_data);
}