#pragma once

#include <Eigen/Dense>
#include <random>
#include <tuple>

using std::vector;

class KMeans
{
    Eigen::MatrixXd const datapoints;
    uint16_t const        k;
    std::minstd_rand      rng;
    ;
    // Each item in the ``clusters`` member corresponds, by index, to a
    // centroid. The contained items are sets of datapoints, again by index,
    // associated to that cluster.
    vector<vector<Eigen::Index>> clusters;
    vector<vector<Eigen::Index>> previous_pass_clusters;
    Eigen::MatrixXd              centroids;

    void randomise_centroids();
    // Returns the total cost of the current cluster assignments.
    double assign_data_to_clusters();
    void   update_centroids();
    // Calculate the total cost of the current datapoint-to-cluster assignment
    // configuration.
    double total_cost(vector<vector<double>> const distances_per_cluster) const;

    public:
    KMeans(Eigen::MatrixXd const datapoints, uint16_t const k, uint const seed);
    std::tuple<vector<vector<Eigen::Index>>, Eigen::Index, double> run(
        uint16_t const repetitions, std::string const trace_file);
};