#include "utils.hpp"
#include <iostream>

Eigen::MatrixXd* utils::upload_samples_to_gpu(Eigen::MatrixXd const& datapoints)
{
    Eigen::MatrixXd* gpu_data  = nullptr;
    auto             amt_bytes = sizeof(double) * datapoints.size();

    cudaMalloc(&gpu_data, amt_bytes);
    cudaMemcpy(gpu_data, datapoints.data(), amt_bytes, cudaMemcpyHostToDevice);

    return gpu_data;
}

void utils::free_samples_on_gpu(Eigen::MatrixXd const* const gpu_datapoint)
{
    cudaFree((void*)gpu_datapoint);
}