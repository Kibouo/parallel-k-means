#include "argument_parser.hpp"
#include "k_means.hpp"
#include "timer.hpp"
#include "utils.hpp"
#include <iostream>

// From: https://eigen.tuxfamily.org/dox/TopicCUDA.html
// workaround issue between gcc >= 4.7 and cuda 5.5
#if (defined __GNUC__) && (__GNUC__ > 4 || __GNUC_MINOR__ >= 7)
#undef _GLIBCXX_ATOMIC_BUILTINS
#undef _GLIBCXX_USE_INT128
#endif

#undef EIGEN_DEFAULT_DENSE_INDEX_TYPE
#define EIGEN_DEFAULT_DENSE_INDEX_TYPE int

int main(int argc, char* argv[])
{
    ArgumentParser cli_args(argc, argv);
    KMeans         kmeans(
        utils::parse_data(cli_args.input()), cli_args.k(), cli_args.seed());

    Timer timer    = Timer();
    auto  clusters = kmeans.run(cli_args.repetitions(), cli_args.trace());
    timer.stop();
    std::cout << timer.duration() << std::endl;

    utils::write_cluster_associations(cli_args.output(), clusters);

    return 0;
}
