#include "k_means.hpp"
#include "utils.hpp"
#include <cmath>
#include <iostream>

int const AMT_THREADS = 256;

__device__ double squared_euclidean_distance(Eigen::VectorXd const& p,
                                             Eigen::VectorXd const& q)
{
    double sum = 0;
    for (Eigen::Index i = 0; i < p.rows(); i++)
    { sum += std::pow(p[i] - q[i], 2); }
    return sum;
}

__global__ void min_distance_and_cluster_per_sample(
    Eigen::Map<Eigen::MatrixXd> const datapoints,
    Eigen::Map<Eigen::MatrixXd> const centroids,
    Eigen::Map<Eigen::VectorXi>       associated_cluster_per_sample,
    Eigen::Map<Eigen::VectorXd>       shortest_distance_per_sample)
{
    extern __shared__ double shared_centroids_data[];
    if (threadIdx.x == 0)
    {
        memcpy(shared_centroids_data,
               centroids.data(),
               centroids.size() * sizeof(double));
    }
    __syncthreads();
    Eigen::Map<Eigen::MatrixXd> shared_centroids = Eigen::Map<Eigen::MatrixXd>(
        shared_centroids_data, centroids.rows(), centroids.cols());

    // Assign each datapoint to the cluster of which the centroid is the
    // closest. Also store the distances to prevent re-calculation (for the
    // total cost) later on.
    Eigen::Index const data_index = blockIdx.x * blockDim.x + threadIdx.x;
    if (data_index >= datapoints.cols()) return;

    size_t closest_centroid_index = 0;
    auto   smallest_distance      = std::numeric_limits<double>::max();
    auto   datapoint              = datapoints.col(data_index);

    for (size_t centroid_index = 0; centroid_index < shared_centroids.cols();
         centroid_index++)
    {
        auto centroid = shared_centroids.col(centroid_index);
        auto distance = squared_euclidean_distance(datapoint, centroid);

        if (distance < smallest_distance)
        {
            closest_centroid_index = centroid_index;
            smallest_distance      = distance;
        }
    }

    associated_cluster_per_sample[data_index] = closest_centroid_index;
    shortest_distance_per_sample[data_index]  = smallest_distance;
}

std::vector<std::vector<double>> const KMeans::assign_data_to_clusters()
{
    auto const      amt_samples = this->datapoints.cols();
    Eigen::VectorXi associated_cluster_per_sample(amt_samples);
    Eigen::VectorXd shortest_distance_per_sample(amt_samples);
    // call kernel
    {
        auto const amt_blocks = amt_samples / AMT_THREADS
                                + ((amt_samples % AMT_THREADS == 0) ? 0 : 1);

        Eigen::MatrixXd* gpu_centroids                     = nullptr;
        Eigen::VectorXi* gpu_associated_cluster_per_sample = nullptr;
        Eigen::VectorXd* gpu_shortest_distance_per_sample  = nullptr;

        auto const centroids_B = sizeof(double) * this->centroids.size();
        auto const associated_cluster_per_sample_B = sizeof(int) * amt_samples;
        auto const shortest_distance_per_sample_B
            = sizeof(double) * amt_samples;

        cudaMalloc(&gpu_centroids, centroids_B);
        cudaMalloc(&gpu_associated_cluster_per_sample,
                   associated_cluster_per_sample_B);
        cudaMalloc(&gpu_shortest_distance_per_sample,
                   shortest_distance_per_sample_B);

        cudaMemcpy(gpu_centroids,
                   this->centroids.data(),
                   centroids_B,
                   cudaMemcpyHostToDevice);

        min_distance_and_cluster_per_sample<<<amt_blocks,
                                              AMT_THREADS,
                                              centroids_B>>>(
            Eigen::Map<Eigen::MatrixXd>((double*)this->gpu_datapoints,
                                        this->datapoints.rows(),
                                        this->datapoints.cols()),
            Eigen::Map<Eigen::MatrixXd>((double*)gpu_centroids,
                                        this->centroids.rows(),
                                        this->centroids.cols()),
            Eigen::Map<Eigen::VectorXi>((int*)gpu_associated_cluster_per_sample,
                                        amt_samples),
            Eigen::Map<Eigen::VectorXd>(
                (double*)gpu_shortest_distance_per_sample, amt_samples));

        cudaMemcpy(associated_cluster_per_sample.data(),
                   gpu_associated_cluster_per_sample,
                   associated_cluster_per_sample_B,
                   cudaMemcpyDeviceToHost);
        cudaMemcpy(shortest_distance_per_sample.data(),
                   gpu_shortest_distance_per_sample,
                   shortest_distance_per_sample_B,
                   cudaMemcpyDeviceToHost);

        cudaFree(gpu_associated_cluster_per_sample);
        cudaFree(gpu_shortest_distance_per_sample);
    }

    auto distances_per_cluster = std::vector<std::vector<double>>(this->k);
    for (Eigen::Index data_index = 0; data_index < amt_samples; data_index += 1)
    {
        auto const closest_centroid_index
            = associated_cluster_per_sample[data_index];
        auto const smallest_distance = shortest_distance_per_sample[data_index];

        distances_per_cluster.at(closest_centroid_index)
            .push_back(smallest_distance);
    }

    // store old clusters for comparison later on
    // NOTE: eigen only copies pointers to the underlying data.
    this->previous_pass_clusters = this->clusters;
    this->clusters               = associated_cluster_per_sample;

    return distances_per_cluster;
}

// __global__ void mean_of_clusters(
//     Eigen::Map<Eigen::MatrixXd> const datapoints,
//     Eigen::Map<Eigen::VectorXi> const associated_cluster_per_sample,
//     Eigen::Map<Eigen::MatrixXd>       centroids)
// {
//     // Loading all datapoints might be too much to fit in the shared memory.
//     extern __shared__ int shared_association_data[];
//     if (threadIdx.x == 0)
//     {
//         memcpy(shared_association_data,
//                associated_cluster_per_sample.data(),
//                associated_cluster_per_sample.size() * sizeof(int));
//     }
//     __syncthreads();
//     Eigen::Map<Eigen::VectorXi> shared_associations
//         = Eigen::Map<Eigen::VectorXi>(shared_association_data,
//                                       associated_cluster_per_sample.size());

//     // Per block we check every sample and process each dimension with a
//     // seperate thread. This way each thread will always be doing the same
//     // (not associated->skip, associated->every thread/dimension adds value).
//     // Every warp will take roughly the same time and not be split in
//     // bussiness internally.
//     // Another option would be to: per block check each dimension.
//     // Per thread another sample is processed. This would result in less work
//     // per thread and more threads (better spread). However, a significant
//     // amount of the threads would be idle. Also, warps might be split in
//     // work-status. This would result in low achieved occupancy.
//     int const cluster_index   = blockIdx.x;
//     int const dimension_index = threadIdx.x;
//     if (cluster_index >= centroids.cols()
//         || dimension_index >= centroids.rows())
//         return;

//     double dimensional_sum_samples = 0.0;
//     int    amt_associated          = 0;
//     for (Eigen::Index sample_index = 0;
//          sample_index < shared_associations.size();
//          sample_index += 1)
//     {
//         if (cluster_index == shared_associations[sample_index])
//         {
//             amt_associated += 1;
//             dimensional_sum_samples
//                 += datapoints.col(sample_index)[dimension_index];
//         }
//     }

//     centroids.col(cluster_index)[dimension_index]
//         = dimensional_sum_samples / (double)amt_associated;
// }

// void KMeans::update_centroids()
// {
//     auto const amt_blocks  = centroids.cols();
//     auto const amt_threads = utils::round_up_to_multiple(
//         centroids.rows(), utils::AMT_THREADS_PER_WARP_CUDA);

//     Eigen::VectorXi* gpu_clusters  = nullptr;
//     Eigen::MatrixXd* gpu_centroids = nullptr;

//     auto const clusters_B  = sizeof(int) * this->clusters.size();
//     auto const centroids_B = sizeof(double) * this->centroids.size();

//     cudaMalloc(&gpu_clusters, clusters_B);
//     cudaMalloc(&gpu_centroids, centroids_B);

//     cudaMemcpy(gpu_clusters,
//                this->clusters.data(),
//                clusters_B,
//                cudaMemcpyHostToDevice);
//     cudaMemcpy(gpu_centroids,
//                this->centroids.data(),
//                centroids_B,
//                cudaMemcpyHostToDevice);

//     mean_of_clusters<<<amt_blocks, amt_threads, clusters_B>>>(
//         Eigen::Map<Eigen::MatrixXd>((double*)this->gpu_datapoints,
//                                     this->datapoints.rows(),
//                                     this->datapoints.cols()),
//         Eigen::Map<Eigen::VectorXi>((int*)gpu_clusters,
//         this->clusters.size()),
//         Eigen::Map<Eigen::MatrixXd>((double*)gpu_centroids,
//                                     this->centroids.rows(),
//                                     this->centroids.cols()));

//     cudaMemcpy(this->centroids.data(),
//                gpu_centroids,
//                centroids_B,
//                cudaMemcpyDeviceToHost);

//     cudaFree(gpu_clusters);
//     cudaFree(gpu_centroids);
// }