#include "k_means.hpp"
#include "utils.hpp"
#include <iostream>
#include <unordered_set>

KMeans::KMeans(Eigen::MatrixXd const&& datapoints,
               uint16_t const          k,
               uint const              seed)
    : datapoints(datapoints), k(k)
{
    this->rng            = std::minstd_rand(seed);
    this->centroids      = Eigen::MatrixXd(datapoints.rows(), k);
    this->gpu_datapoints = utils::upload_samples_to_gpu(datapoints);
}

Eigen::VectorXi const KMeans::run(uint16_t const     repetitions,
                                  std::string const& trace_file)
{
    auto            smallest_cost = std::numeric_limits<double>::max();
    Eigen::VectorXi best_solution;

    for (size_t i = 0; i < repetitions; i++)
    {
        std::vector<std::vector<double>> distances_per_cluster;
        size_t                           nth_iteration = 0;

        this->randomise_centroids();
        this->clusters = Eigen::VectorXi(this->datapoints.cols());
        do
        {
            distances_per_cluster = this->assign_data_to_clusters();
            this->update_centroids();

            if (trace_file != "" && i == 0)
            {
                utils::write_cluster_associations(
                    trace_file, this->clusters, nth_iteration > 0);
            }

            nth_iteration++;
        }    // TODO: add limiter in case of infinite stealing loop
        while (this->clusters != this->previous_pass_clusters);

        auto cost = this->total_cost(std::move(distances_per_cluster));
        if (cost < smallest_cost)
        {
            smallest_cost = cost;
            best_solution = std::move(this->clusters);
        }
    }

    return best_solution;
}

void KMeans::randomise_centroids()
{
    std::unordered_set<uint> cluster_indices;

    for (Eigen::Index centroid_index = 0;
         centroid_index < this->centroids.cols();
         centroid_index += 1)
    {
        auto centroid = this->centroids.col(centroid_index);
        // make sure not to accidentally assign the same data entry to 2
        // clusters
        uint_fast32_t index;
        auto          unique_index_found = false;
        while (!unique_index_found)
        {
            index = this->rng() % (uint_fast32_t)this->datapoints.cols();
            if (cluster_indices.find(index) == cluster_indices.end())
            {
                cluster_indices.insert(index);
                unique_index_found = true;
            }
        }

        centroid = this->datapoints.col(index);
    }
}

void KMeans::update_centroids()
{
    std::vector<std::vector<Eigen::Index>> non_linear_clusters(this->k);
    for (Eigen::Index data_index = 0; data_index < this->clusters.size();
         data_index += 1)
    {
        auto const cluster_association = this->clusters[data_index];
        non_linear_clusters.at(cluster_association).push_back(data_index);
    }

    // For each cluster we calculate the mean of the datapoints and use
    // that as new centroid.
    for (Eigen::Index centroid_index = 0;
         centroid_index < this->centroids.cols();
         centroid_index++)
    {
        auto cluster_size = non_linear_clusters.at(centroid_index).size();
        auto summed_datapoints
            = Eigen::VectorXd(this->centroids.rows()).setZero();

        for (auto const& datapoint_index :
             non_linear_clusters.at(centroid_index))
        { summed_datapoints += this->datapoints.col(datapoint_index); }

        this->centroids.col(centroid_index) = summed_datapoints / cluster_size;
    }
}

double KMeans::total_cost(
    std::vector<std::vector<double>> const&& distances_per_cluster) const
{
    double sum = 0;
    for (auto const& distances : distances_per_cluster)
    {
        for (auto const& distance : distances) { sum += distance; }
    }

    return sum;
}