#pragma once

#include <Eigen/Dense>
#include <vector>

namespace utils
{
    // cc 3.0 -> 7.5
    int const MAX_THREADS_PER_BLOCK_CUDA = 1024;
    int const MAX_BLOCKS_PER_X_GRID_CUDA = pow(2, 31) - 1;
    int const AMT_THREADS_PER_WARP_CUDA  = 32;

    /// Returns a matrix in which each row is a datapoint. We store datapoints
    /// per column.
    Eigen::MatrixXd const parse_data(std::string const& file_name);

    void write_cluster_associations(std::string const&     file_name,
                                    Eigen::VectorXi const& clusters,
                                    bool const             append = false);

    // Allocates memory on the gpu for the sample matrix and copies it over.
    // This is done only 1ce throughout te application.
    Eigen::MatrixXd* upload_samples_to_gpu(Eigen::MatrixXd const& datapoints);
    void free_samples_on_gpu(Eigen::MatrixXd const* const gpu_datapoint);

    int round_up_to_multiple(int const value, int const multiple);
}