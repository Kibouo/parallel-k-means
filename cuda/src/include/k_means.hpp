#pragma once

#include "utils.hpp"
#include <Eigen/Dense>
#include <random>
#include <tuple>

class KMeans
{
    Eigen::MatrixXd const  datapoints;
    Eigen::MatrixXd const* gpu_datapoints;
    uint16_t const         k;
    std::minstd_rand       rng;

    // Each index of the ``clusters`` member corresponds to the index of a
    // sample. The contained value is the associated cluster.
    Eigen::VectorXi clusters;
    Eigen::VectorXi previous_pass_clusters;
    Eigen::MatrixXd centroids;

    void randomise_centroids();
    // Returns, per cluster, the distances of the datapoints to their respective
    // centroids.
    std::vector<std::vector<double>> const assign_data_to_clusters();
    void                                   update_centroids();
    // Calculate the total cost of the current datapoint-to-cluster assignment
    // configuration.
    double total_cost(
        std::vector<std::vector<double>> const&& distances_per_cluster) const;

    public:
    KMeans(Eigen::MatrixXd const&& datapoints,
           uint16_t const          k,
           uint const              seed);
    inline ~KMeans() { utils::free_samples_on_gpu(this->gpu_datapoints); };
    Eigen::VectorXi const run(uint16_t const     repetitions,
                              std::string const& trace_file);
};