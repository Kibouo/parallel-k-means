#include "utils.hpp"
#include "CSVReader.hpp"
#include "CSVWriter.hpp"
#include <cmath>

Eigen::MatrixXd const utils::parse_data(std::string const& file_name)
{
    std::ifstream file(file_name);
    file.exceptions(std::ios::badbit);
    CSVReader reader(file);

    auto            first_read = true;
    Eigen::MatrixXd columns_of_datapoints;
    Eigen::VectorXd datapoint;
    while (reader.read(datapoint))
    {
        if (first_read)
        {
            columns_of_datapoints.conservativeResize(datapoint.rows(), 1);
            first_read = false;
        }
        else
        {
            columns_of_datapoints.conservativeResize(
                Eigen::NoChange, columns_of_datapoints.cols() + 1);
        }

        columns_of_datapoints.col(columns_of_datapoints.cols() - 1)
            = std::move(datapoint);
    }

    file.close();
    return columns_of_datapoints;
}

void utils::write_cluster_associations(std::string const&     file_name,
                                       Eigen::VectorXi const& clusters,
                                       bool const             append)
{
    std::vector<Eigen::Index> datapoint_cluster_association(
        clusters.data(), clusters.data() + clusters.size());

    // actually write it to file
    if (append)
    {
        std::ofstream file(file_name, std::ios_base::app);
        CSVWriter     writer(&file);
        writer.write(datapoint_cluster_association);
        file.close();
    }
    else
    {
        std::ofstream file(file_name);
        CSVWriter     writer(&file);
        writer.write(datapoint_cluster_association);
        file.close();
    }
}

int utils::round_up_to_multiple(int const value, int const multiple)
{
    if (multiple == 0) return value;

    auto const non_multiple_part = value % multiple;
    if (non_multiple_part == 0) return value;

    return value - non_multiple_part + multiple;
}