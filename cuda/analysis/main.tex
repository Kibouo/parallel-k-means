\documentclass[a4paper, 11pt]{article} 

\title{Analyse K-means:\\CUDA} 
\author{Mihály Csonka (1644219)} 
\date{\today}

\usepackage[parfill]{parskip}
\usepackage[
    style=numeric,
    backend=biber,
    isbn=true,
    url=true,
    datamodel=archive
]{biblatex}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{caption}
\usepackage{csquotes}
\usepackage[toc,page]{appendix}
\usepackage[hidelinks]{hyperref}
\usepackage{geometry}
\geometry{margin=1.11in}
\hyphenpenalty=10000

\begin{document} 
\maketitle

\section{Parallelisation choices}
Coming up with a fitting way to map data indexing to GPU blocks and threads was a difficult task. When done carelessly it results in too much and random/un-aligned global memory access, inability to use shared memory, and requiring post-processing.
Another huge performance sink is copying big amounts of data to and from GPU memory. To prevent this as much as possible the immutable dataset is copied only once, at the initialisation of the program, to the GPU.

Considering the above, assisted by profiling, we made following parallelisation choices.

\subsection{Parallelised: assigning data to clusters}
The computationally most expensive part of the algorithm is calculating the distance between samples and each centroid to then assign these samples to the closest cluster.
This does not require randomly accessing of data and could thus take advantage of memory access coalescing. Small parts of the data, more specifically the centroids, are visited multiple times and could be stored in shared memory for lower latency. This is why we decided to parallelise this part of the algorithm.

Our initial approach was to map each dimension of each sample to a thread. We would then load the centroids into shared memory. Having centroids locally would be even faster, but depending on the parameters and sample dimensionality it would result in either insufficient memory or it would limit the amount of blocks per SM.
Eventually each thread would calculate the distance to each centroid's corresponding dimension and store this in global memory. However, this would require visiting each \newline\texttt{n\_samples * k\_clusters * d\_sample\_dimension} distance data point during post-processing. The post-processing would entail reducing the dimensional distances to per sample-centroid pair distances. These in turn would be used to deduce a list of associated clusters per sample. This could be done either by thread 0 of a warp while the rest is idle, or on the CPU.

To remove the need for post-processing we decided that each sample is instead mapped to a thread. A usual dataset has enough samples to provide decent occupancy for the GPU.
Centroids are still loaded into shared memory. However, each thread now does the calculation of the distance between sample and each centroid over all dimensions. It also does the reduction in one go. This approach also simplifies the code.

\subsection{Not parallelised}
Following functions were not parallelised for given reasons.

\subsubsection{Repetitions}
Opposed to OpenMP where we parallelised each repetition of the k-means algorithm, in our CUDA implementation this has not been done. It is hard to imagine how the serial k-means algorithm would be mapped to a kernel. As a repetition is serial, there is no benefit to assigning several threads to a single repetition. When assigning each repetition to a single thread, given that the amount of repetitions is usually a factor of 10 or 100, the occupancy of the would be quite low.

\subsubsection{Updating centroid}
Calculating centroids given the samples and their new associations was an interesting function to examine.

Given that each sample has to be processed, and the amount of samples is usually big, with correct block dimensions the occupancy would be high. This could also be done in such way that global memory access is done in an optimal manner. There is also a set of data that every thread could share to reduce memory access latency, namely the cluster association per sample. Furthermore, by moving this function to a CUDA kernel the centroids could be mutated and read on the GPU without ever needing to be copied back to the host.

Given the promising possibilities, we implemented the kernel as follows. The samples' cluster associations are stored in shared memory. Each block is assigned a cluster index and each thread herein is assigned a sample's dimension. Each thread then checks every sample. When the sample belongs to the block's cluster, the thread sums the sample's dimension to a total for that dimension and for that cluster. It also increments the amount of samples associated with the given cluster. At the end it stores the mean for the corresponding cluster and dimension in the global centroids matrix.

We implemented this but saw surprising results. The algorithm ran several seconds slower than without this new kernel.\footnote{For an implementation see the commented out function \texttt{update\_centroids()} in \texttt{k\_means.cu}.} When profiling the code we noticed that only \~{}2.6\% of the time was spent in this kernel, while the other majority of the time was spent in the kernel which calculates distances and new cluster associations.
Admittedly, our implementation did not yet keep the centroids on the GPU. Memory transfer between host and GPU is relatively slow. However, for the given small amounts of data (centroids) the memory transfer is, relative to the kernel's execution time, non-existent. This is shown in the profiling results visible in Figure \ref{min_dist_perf} and \ref{memcpy_perf} respectively.\footnote{This profiling was done with CUDA's \texttt{nvvp} utility on the \texttt{simple1} dataset with \texttt{k = 5}. Results are close to identical for other parameters however.} With tweaked parameters, resulting in more centroid data, we might barely break even or improve only slightly by keeping the centroids on the GPU. We expected the kernel in itself to give substantial improvements.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.44]{min_dist_perf.png}
    \caption{Performance profile of a kernel invocation which updates centroids.}\label{min_dist_perf}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.45]{memcpy_perf.png}
    \caption{Performance profile of a \texttt{memcpy} invocation that copies centroids from device to host, right after the kernel invocation from Figure \ref{min_dist_perf}.}\label{memcpy_perf}
\end{figure}

\section{Speed up per core count}
We tested the 4 combinations of scenarios for \texttt{k = 20} and \texttt{k = 30}, and 20 and 30 repetitions. Each of the scenarios was repeated for the thread counts per block in the range [256, 768], using a step size of 128, on the \texttt{simple1} dataset. Each of these tests was then repeated 6 times.

In Figure \ref{fig_speedup} we show the speedup for the last listed scenario. However, we note that each scenario returned similar results. Due to the inherent parallel design of the GPU, the speedup is roughly the same across the board. That said, a slight decrease in speedup given an increasing number of threads per block is observable. Except for 768 threads where the speedup slowly rises again.

The slight differences are the result of different levels of occupancy. By tweaking the number of warps per block, the resource usage per block changes. As each SM has a limited set of resources, more `expensive' blocks decrease the amount of possible concurrent blocks per SM. E.g.\ a SM with a hypothetical maximum of 16 warps can hold either 2 blocks of 8 warps or 1 block of 9 warps. 

In our application more threads per block means more concurrent shared memory accesses. To offset this a good distribution of concurrent blocks/warps is needed to hide the increased latency/idle warps. Finding the right balance is thus crucial. The given number of threads just so happen to have a worse occupancy when increasing, up till 768 which again results in a slightly better distribution of warps/blocks over the available SMs.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.45]{fig_speedup.png}
    \caption{Speedup differences per thread count of a block.}\label{fig_speedup}
\end{figure}

\section{Serial fraction}\label{serial_fraction}
We used the \texttt{simple1} dataset with \texttt{k = 30}, 30 repetitions, and 256 threads per block. The dataset contains 5000 samples and our only kernel processes each in an individual thread. This results in 20 blocks. For the first run of this setup we got:
\(T_s = 0.621s\), \(T_p = 154.547s\), and as result \(f = 0.996\).

\section{Parallel overhead}
To calculate the overhead we doubled the amount of threads per block to 512. As the amount of work stays constant this results in 10 blocks. In total there are 5120 threads, just as in Section \ref{serial_fraction}. We would thus not expect any difference in execution time. However, the result of run 1 for 512 threads per block is \(169.092s\). \newline This results in \(T_o = 169.092s - 155.168s = 13.924s\).\footnote{We note that the execution times per scenario over multiple runs were quite consistent. The differences between runs were in an order of magnitude of \(0.1s\).} This is obviously not 0 and shows that Amdahl's law has its flaws.

The theoretical occupancy, e.g.\ based on GPU properties such as maximum number of threads per SM, for 512 threads is the same as with 256 threads. The overhead is thus most likely the result of shared memory being accessed by more threads which results in more conflicts.

\section{Predictions}
We interpret this question as follows in the case of CUDA: what impact would more/unlimited resources per SM, as well as more/unlimited SMs have for the performance of the application? We reiterate that our only kernel calculates distances and assigns new cluster associations. It is modelled to map each sample to a separate thread.

Increasing the amount of threads per block has shown to not always be beneficial. Being able to process all 5000 samples, in the case of the \texttt{simple1} dataset, in the same block/SM would not benefit our implementation much. Memory access would simply be increased, resulting in more conflicts. Due to more warps being available, latency hiding would practically always be a possibility however.

On the other hand, increasing the amount of SMs would be greatly beneficial. We could use one warp per block/SM and spread out the data in groups of 32 samples per SM. There would be no latency hiding possible due to there being no idle warps. However, there would also be minimal shared memory conflicts. Furthermore, all the sets of 32 samples would be processed in parallel. Having unlimited SMs would effectively remove the amount of samples, which would now be a constant, from the time complexity of this quite heavy and big (99.6\%!) part of our k-means implementation.

\section{Possible further improvements}
Currently the copying of memory between host and device is relatively non-existent. However, would the copying evolve to be more time consuming we could enable the \texttt{update\_centroids()} kernel and patch the code as to keep centroids on the GPU at all times. While the kernel itself would not improve execution time, this way the expensive copying of data would be prevented.

\end{document}
