#!/usr/bin/env python
import matplotlib.pyplot as plt
import glob
import numpy as np

# Load all data into a single numpy array
allData = [ ]
for fn in glob.glob("speedup_out_*.txt"):
    data = [ list(map(float,l.split())) for l in open(fn).readlines() ]
    allData.append(data)
    
allData = np.array(allData);

# The number of processes is present in each dataset,
# just keep one of them for plotting
numProc = allData[:,:,0][0]
print("Number of processes: ", numProc)

# For all times we'll calculate both the average and standard deviation
times = allData[:,:,1]
avgTimes = np.average(times, 0)
stdDevTimes = np.std(times, 0)

# Plot execution times
plt.figure(figsize=(7,7))
plt.errorbar(numProc, avgTimes, stdDevTimes, marker='.', capsize=5);
plt.gca().set_xlabel("Aantal threads")
plt.gca().set_ylabel("Uitvoertijd (s)")
plt.savefig("fig_uitvoertijd.png", bbox_inches="tight", dpi=200)

# Plot execution times as well as best case execution times. For simplicity 
# we're using the average serial time here as a reference.
plt.figure(figsize=(7,7))
plt.plot(numProc, avgTimes[0]/numProc, label="Theoretische curve");
plt.errorbar(numProc, avgTimes, stdDevTimes, marker='.', capsize=5, label="Gemeten curve");
plt.legend()
plt.gca().set_xlabel("Aantal threads")
plt.gca().set_ylabel("Uitvoertijd (s)")
plt.savefig("fig_uitvoertijd2.png", bbox_inches="tight", dpi=100)

# Same plot, but zoomed in
plt.figure(figsize=(7,7))
plt.plot(numProc, avgTimes[0]/numProc, label="Theoretische curve");
plt.errorbar(numProc, avgTimes, stdDevTimes, marker='.', capsize=5, label="Gemeten curve");
plt.legend()
plt.gca().set_ylim([0,1]) # Here we're specifying a specific part of the plot
plt.gca().set_xlabel("Aantal threads")
plt.gca().set_ylabel("Uitvoertijd (s)")
plt.savefig("fig_uitvoertijd2_zoom.png", bbox_inches="tight", dpi=100)

# Difference of the above two curves
plt.figure(figsize=(7,7))
expected = avgTimes[0]/numProc
plt.errorbar(numProc, avgTimes - expected, stdDevTimes, marker='.', capsize=5);
plt.gca().set_xlabel("Aantal threads")
plt.gca().set_ylabel("Verschil in uitvoertijd (s)")
plt.savefig("fig_uitvoertijd2_zoom_verschil.png", bbox_inches="tight", dpi=100)

# The speedup is the first time (the serial case) divided by the execution
# times. For simplicity we're using the average serial time here as a 
# reference.
speedups = avgTimes[0]/times

# Calculate average and standard deviation for the speedup
avgSpeedups = np.average(speedups, 0)
stdDevSpeedups = np.std(speedups, 0)

plt.figure(figsize=(8,8))
plt.plot(numProc, numProc, label = "Maximale speedup")
plt.errorbar(numProc, avgSpeedups, stdDevSpeedups, marker='.', capsize=5, label="Gemeten speedup");
plt.legend()
plt.gca().set_xlabel("Aantal threads")
plt.gca().set_ylabel("Speedup factor")
plt.savefig("fig_speedup.png", bbox_inches="tight", dpi=200)

# Calculate the efficienties, their average and standard deviation
# in a similar way, and plot it.
efficiencies = speedups/numProc
avgEfficiencies = np.average(efficiencies, 0)
stdDevEfficiencies = np.std(efficiencies, 0)

plt.figure(figsize=(8,8))
plt.plot(numProc, [1]*len(numProc), label="Maximale efficiëntie")
plt.errorbar(numProc, avgEfficiencies, stdDevEfficiencies, marker='.', capsize=5, label="Gemeten efficiëntie")
plt.legend()
plt.gca().set_ylim([0,1.15])
plt.gca().set_xlabel("Aantal threads")
plt.gca().set_ylabel("Efficiëntie")
plt.savefig("fig_efficiency.png", bbox_inches="tight", dpi=200)

