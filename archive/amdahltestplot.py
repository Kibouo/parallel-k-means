#!/usr/bin/env python
import matplotlib.pyplot as plt
import glob
import numpy as np

# Load all data into a single numpy array
allData = [ ]
for fn in glob.glob("speedup_out_*.txt"):
    data = [ list(map(float,l.split())) for l in open(fn).readlines() ]
    allData.append(data)
    
allData = np.array(allData)

# The number of processes is present in each dataset,
# just keep one of them for plotting
numProc = allData[:,:,0][0]
print("Number of processes: ", numProc)

# For all times we'll calculate both the average and standard deviation
times = allData[:,:,1]
avgTimes = np.average(times, 0)
stdDevTimes = np.std(times, 0)

# The speedup is the first time (the serial case) divided by the execution
# times. For simplicity we're using the average serial time here as a 
# reference.
speedups = avgTimes[0]/times

# Calculate average and standard deviation for the speedup
avgSpeedups = np.average(speedups, 0)
stdDevSpeedups = np.std(speedups, 0)

# Plot everything
plt.figure(figsize=(8,8))
plt.plot(numProc, numProc, label = "Maximale speedup")
plt.errorbar(numProc, avgSpeedups, stdDevSpeedups, marker='.', capsize=5, label="Gemeten speedup");

s = 0.036
maxSpeedup = 1/s
plt.plot([1,64], [maxSpeedup,maxSpeedup], label = "Speedup 1/s = {:.1f}".format(maxSpeedup))
plt.gca().set_xlabel("N")
plt.gca().set_ylabel("Speedup")

x = np.linspace(1,64,100)
theoretisch = 1/(s+(1-s)/x)
plt.plot(x, theoretisch, label = "Theoretisch voor s = {}".format(s))
plt.legend()
plt.savefig("amdahltest.png")

