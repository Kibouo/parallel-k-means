##!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import sys
import argparse

def main():
    parser = argparse.ArgumentParser(description='Print trace for K-means run.')

    parser.add_argument('data', metavar='data', type=str, help='csv file with data points.')
    parser.add_argument('trace', metavar='trace', type=str, help='csv file with trace')

    args = parser.parse_args()

    X = np.loadtxt(args.data, delimiter=',')
    trace = np.loadtxt(args.trace, delimiter=',').astype(int)

    fig = plt.figure()

    # cols = rows = np.ceil(np.sqrt(trace.shape[0]))
    cols = trace.shape[0]
    rows = 1
    for i in range(trace.shape[0]):
        ax = fig.add_subplot(rows, cols, i + 1)
        plot_clustering(ax, trace[i, :], X, "Iteratie %s" % i)

    plt.show()

def plot_clustering(ax, trace, X, title):
    color = np.array(['red', 'green', 'blue', 'black', 'cyan', 'yellow', 'orange'])

    ax.set_title(title)
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)

    col = color[trace]
    ax.scatter(X[:, 0], X[:, 1], c = col, marker = '.', s = 5, alpha=0.5)

    centroid_names = list(set(trace))
    centroids = np.empty((len(centroid_names), X.shape[1]))

    for j, e in enumerate(centroid_names):
        centroids[j, :] = np.mean(X[trace == e, :], axis=0)

    ax.scatter(centroids[:, 0], centroids[:, 1], marker = '+', s=64, c=color[centroid_names])

if __name__ == '__main__':
    main()
